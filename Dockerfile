FROM arm32v7/php:8.0-rc-fpm-alpine

RUN apk --no-cache add shadow && \
	usermod -u 55 xfs && \
	groupmod -g 55 xfs && \
	usermod -u 33 www-data && \
	groupmod -g 33 www-data

# repository for easy php extension installation 
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/


RUN chmod +x /usr/local/bin/install-php-extensions &&  \
    install-php-extensions imagick zip mysqli bcmath exif


