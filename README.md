# PHP docker image build on RPI 4 - armv7 (32 bit architecture) ready for Wordpress

PHP image build from base with addtional extensions to make it ready for Wordpress use. The build image can be found at [dockerhub](https://hub.docker.com/repository/docker/hunnguye/php-wordpressready-armv7).

Additional modules include:

* mysqli
* bcmath
* exif
* imagick
* zip

Also the UID/GUID the image has been changed. The original image mapped www-data to 82. This image configured www-data to be mapped to 33 to be the same as host UID/GUID.

I have been using it exclusively for my [Dockerized Wordpress on Raspberry Pi project](https://gitlab.com/nm_hung93/dockerized-wordpress-on-raspberry-pi).

If you have any suggestions or other requirements, feel free to create an issue and i might be able to resolve it or build a custom image for you :)

Also shoutout to [mlocati](https://github.com/mlocati/docker-php-extension-installer) for providing such an easy way to install additional php modules.
